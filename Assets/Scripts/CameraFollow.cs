﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    Transform character;
    [SerializeField]
    [Range(0, 1)]
    float margin = .1f;

    Camera myCamera;

    void Awake()
    {
        myCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void LateUpdate() {

        Vector3 viewpointPosition = myCamera.WorldToViewportPoint(character.position);

        if (viewpointPosition.x < margin)
        {
            viewpointPosition.x = margin;
        } else if (viewpointPosition.x > 1.0f - margin)
        {
            viewpointPosition.x = 1.0f - margin;
        }

        if (viewpointPosition.y < margin)
        {
            viewpointPosition.y = margin;
        }
        else if (viewpointPosition.y > 1.0f - margin)
        {
            viewpointPosition.y = 1.0f - margin;
        }

        Vector3 nearestPointOnMargin = myCamera.ViewportToWorldPoint(viewpointPosition);

        transform.position += character.position - nearestPointOnMargin;
    }
}

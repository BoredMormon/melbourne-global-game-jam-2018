﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFinder : MonoBehaviour
{

    Movement character;

    // Use this for initialization
    void Start()
    {
        character = GameObject.Find("Character").GetComponent<Movement>();
    }

    public void SpeedUpCharacter(float speed)
    {
        character.speed = speed;
    }
}

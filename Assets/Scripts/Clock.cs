﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour {

    [SerializeField]
    String epoch = "11:00";
    [SerializeField]
    float speed = 1.0f;

    float levelStartTime;
    float epochInSeconds;
    Text text;
    public float currentTime { get; private set; }

	// Use this for initialization
	void Start ()
    {
        TimeSpan offsetFromUnixEpoch = DateTime.Parse(epoch).Subtract(DateTime.Parse("00:00"));
        epochInSeconds = (float) offsetFromUnixEpoch.TotalSeconds;
        levelStartTime = Time.time * speed;
        text = GetComponent<Text>();
        currentTime = calculateCurrentTime();
    }
	
    private float calculateCurrentTime()
    {
        return (Time.time * speed - levelStartTime) + epochInSeconds;
    }

	// Update is called once per frame
	void Update () {
        currentTime = calculateCurrentTime();
        int hour = (int)(currentTime / 60 / 60) % 24;
        int minute = (int)(currentTime / 60) % 60;
        String separator;
        if (((int) Time.unscaledTime) % 2 == 0)
        {
            separator = ":";
        } else
        {
            separator = " ";
        }

        String suffix;
        if (hour >= 12)
        {
            suffix = "P";
        } else
        {
            suffix = "A";
        }
        if (hour == 0)
        {
            hour = 12;
        } else if (hour > 12)
        {
            hour -= 12;
        }
        text.text = String.Format("{0:d2}{1}{2:d2}{3}", hour, separator, minute, suffix);
	}
}

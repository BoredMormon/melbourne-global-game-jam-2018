﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EventOnAwake : MonoBehaviour {

    [SerializeField]
    UnityEvent eventOnAwake;

	// Use this for initialization
	void Awake () {
        eventOnAwake.Invoke();
	}
}

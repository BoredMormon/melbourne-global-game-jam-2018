﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnTimer : MonoBehaviour {

    [SerializeField]
    float timer;

    [SerializeField]
    UnityEvent eventToTrigger;

	// Use this for initialization
	void Start () {
        StartCoroutine(TriggerEvent());
	}
	
	IEnumerator TriggerEvent()
    {
        yield return new WaitForSeconds(timer);
        eventToTrigger.Invoke();
    }
}

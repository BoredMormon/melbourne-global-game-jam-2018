﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnTrigger : MonoBehaviour {

    [SerializeField]
    UnityEvent eventOnEnter;

    [SerializeField]
    UnityEvent eventOnExit;

    // Use this for initialization
    void OnTriggerEnter2D()
    {
        eventOnEnter.Invoke();
    }

    void OnTriggerExit2D()
    {
        eventOnExit.Invoke();
    }
}

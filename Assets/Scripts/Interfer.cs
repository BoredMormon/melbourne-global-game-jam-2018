﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Interfer : MonoBehaviour {

    int station = -1;
    AudioSource myAudio;

    bool interfering = false;

    StationMixer stationMixer;
    AudioMixer myMixer;

    void Awake()
    {
        stationMixer = GameObject.Find("Radio Stations").GetComponent<StationMixer>();
        myAudio = GetComponent<AudioSource>();
        station = int.Parse(myAudio.outputAudioMixerGroup.audioMixer.name.Split(' ')[1]);

        myMixer = myAudio.outputAudioMixerGroup.audioMixer;

    }


    public void InterferNow()
    {
        stationMixer.StartInterference(station);
        interfering = true;
    }

    public void StopInterferNow()
    {
        stationMixer.StopInterference();
        interfering = false;
        myMixer.SetFloat("Interupt", -80);
        myMixer.SetFloat("Program", 0);

    }

    void Update()
    {
        if (interfering)
        {
            myMixer.SetFloat("Interupt", 0);
            myMixer.SetFloat("Program", -80);
        }
        else
        {
            myMixer.SetFloat("Interupt", -80);
            myMixer.SetFloat("Program", 0);
        }
    }
}

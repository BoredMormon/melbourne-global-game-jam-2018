﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    Rigidbody2D myRigidbody;
    [SerializeField]
    public float speed = 1;
    [SerializeField]
    [Range(0, 1)]
    float deadZone = .1f;

    Animator animator;

    enum MoveState {up, left, right, down, idle}
    MoveState lastState = MoveState.idle;
    MoveState newState = MoveState.idle;

    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        myRigidbody.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed * 0.75f);

        if (Input.GetAxis("Horizontal") < -deadZone)
        {
            newState = MoveState.left;
        } else if(Input.GetAxis("Horizontal") > deadZone)
        {
            newState = MoveState.right;
        }
        else if(Input.GetAxis("Vertical") > deadZone)
        {
            newState = MoveState.up;
        }
        else if(Input.GetAxis("Vertical") < -deadZone)
        {
            newState = MoveState.down;
        }
        else 
        {
            newState = MoveState.idle;
        }



        if (newState != lastState) {
            switch (newState)
            {
                case MoveState.left:
                    animator.SetTrigger("Left");
                    break;
                case MoveState.right:
                    animator.SetTrigger("Right");
                    break;
                case MoveState.up:
                    animator.SetTrigger("Up");
                    break;
                case MoveState.down:
                    animator.SetTrigger("Down");
                    break;
                case MoveState.idle:
                    animator.SetTrigger("Idle");
                    break;
            }
            lastState = newState;
        }
            
    }
}

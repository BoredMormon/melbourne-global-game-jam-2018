﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PowerBoxInterupter : MonoBehaviour {


    [SerializeField]
    List<AudioMixer> allStations = new List<AudioMixer>();

    [SerializeField]
    GameObject staticAudioSourceGameObject;

    [SerializeField]
    float staticGain;

    void OnTriggerStay2D()
    {
        foreach (AudioMixer myMixer in allStations)
        {
            myMixer.SetFloat("Powerbox", staticGain);
            myMixer.SetFloat("Interupt", -80);
            myMixer.SetFloat("Program", -80);
            staticAudioSourceGameObject.SetActive(true);
        }
    }

    void OnTriggerExit2D()
    {
        foreach (AudioMixer myMixer in allStations)
        {
            myMixer.SetFloat("Program", 0);
            myMixer.SetFloat("Interupt", 0);
            myMixer.SetFloat("Powerbox", -80);
            staticAudioSourceGameObject.SetActive(false);
        }
    }
}

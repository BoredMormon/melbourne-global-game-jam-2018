﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SequentialAudio : MonoBehaviour {

    [SerializeField]
    List<DelayedAudio> tracks = new List<DelayedAudio>();

    [SerializeField]
    UnityEvent eventWhenFinished;

    AudioSource myAudio;

    void Start ()
    {
        myAudio = GetComponent<AudioSource>();
        StartCoroutine(PlayAudioSequence());
    }

    IEnumerator PlayAudioSequence()
    {
        foreach(DelayedAudio track in tracks)
        {
            yield return new WaitForSeconds(track.delayBefore);
            myAudio.clip = track.audio;
            myAudio.Play();
            while (myAudio.isPlaying) yield return null;
        }
        yield return null;
        eventWhenFinished.Invoke();
    }

}

[System.Serializable]
public struct DelayedAudio
{
    public AudioClip audio;
    public float delayBefore;
}

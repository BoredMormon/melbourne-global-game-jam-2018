﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SliderMessage : MonoBehaviour {

    [SerializeField]
    UnityEventFloat target;

	// Use this for initialization
	void Start () {
        float value = GetComponent<Slider>().value;
        target.Invoke(value);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

[System.Serializable]
public class UnityEventFloat : UnityEvent<float> { }
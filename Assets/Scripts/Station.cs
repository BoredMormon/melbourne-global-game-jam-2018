﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : MonoBehaviour {

    [SerializeField]
    List<AudioClip> tracks;

    AudioSource myAudioSource;

    void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!myAudioSource.isPlaying)
        {
            myAudioSource.clip = tracks[Random.Range(0, tracks.Count)];
            myAudioSource.Play();
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

public class StationMixer : MonoBehaviour {

    [SerializeField]
    AudioMixer stationMixer;

    [SerializeField]
    [Range(0, 1)]
    float liveBandPercent = 0.1f;
    int station;

    public bool isTuned { get; private set; }

    bool interfering = false;
    int clearChanel;

    List<AudioMixerGroup> stations = new List<AudioMixerGroup>();


    void Awake()
    {
        AudioMixerGroup[] groups = stationMixer.FindMatchingGroups("");

        for (int i = 0; i < groups.Length; i++)
        {
            if (groups[i].name.Contains("Station"))
            {
                stations.Add(groups[i]);
            }
        }
    }

    // p should be an odd positive integer. 11 works pretty well.
    // https://www.desmos.com/calculator/sna1jmmisa
    private float flatten(float x, float p)
    {
        float v = Mathf.Sign(x) * Mathf.Pow(x, p);
        return Mathf.Pow(1f - v, p);
    }

    private float toDecibels(float x)
    {
        return (1 - x) * -80;
    }

    public void SetFrequency(float frequency)
    {
        frequency = FrequencyToStationSpace(frequency);
        float volume = Mathf.Cos(Mathf.PI * frequency);
        float staticVolume = Mathf.Cos(Mathf.PI * (frequency + 0.5f));
        volume = flatten(volume, 9f);
        staticVolume = flatten(staticVolume, 13f);
        station = Mathf.CeilToInt(frequency);
        Debug.Log(String.Format("{0} {1} {2}", volume, staticVolume, station));
        isTuned = volume > liveBandPercent;

        for (int i = 1; i <= stations.Count; i++)
        {
            if (i == station)
            {
                stationMixer.SetFloat("Station " + i, toDecibels(volume));
            } else
            {
                stationMixer.SetFloat("Station " + i, -80);
            }
        }
        stationMixer.SetFloat("Static", toDecibels(staticVolume) + 12.0f);
        EventSystem.current.SetSelectedGameObject(null);
    } 
    
    void Update () {
        if (interfering && station != clearChanel)
        {
            stationMixer.SetFloat("Interference", 0);
        } else
        {
            stationMixer.SetFloat("Interference", -80);
        }

    }

    float FrequencyToStationSpace (float frequency)
    {
        return frequency * stations.Count;
    }

    public void StartInterference (int clearChannel)
    {
        interfering = true;
        this.clearChanel = clearChannel;
    }

    public void StopInterference ()
    {
        interfering = false;
    }

}

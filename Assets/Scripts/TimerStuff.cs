﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerStuff : MonoBehaviour {

	// Use this for initialization
	public void Pause () {
        Time.timeScale = 0;
	}
	
	// Update is called once per frame
	public void UnPause () {
        Time.timeScale = 1;
	}
}

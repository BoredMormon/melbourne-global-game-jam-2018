﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour {

	[SerializeField]
    UnityEvent enterTriggerEvent;

    [SerializeField]
    UnityEvent exitTriggerEvent;


    void OnTriggerEnter2D()
    {
        enterTriggerEvent.Invoke();
    }

    void OnTriggerExit2D()
    {
        exitTriggerEvent.Invoke();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuningIndicator : MonoBehaviour {

    [SerializeField]
    StationMixer mixer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (mixer.isTuned)
        {
            transform.Find("Indicator On").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("Indicator On").gameObject.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

public class Volume : MonoBehaviour {

    [SerializeField]
    AudioMixer finalMixer;

    public void SetVolume(float volume)
    {
        float volumeDb = Mathf.Lerp(-60.0f, 0.0f, volume);
        float ambientDB = Mathf.Lerp(0.0f, -60.0f, volume);
        finalMixer.SetFloat("Station", volumeDb);
        finalMixer.SetFloat("Ambient", ambientDB);
        EventSystem.current.SetSelectedGameObject(null);
    }
}
